<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table='articulos';
    public $fillable=['titulo','descripcion'];
}
